function mapObject(obj, cb) {
    if (obj && cb) {
        for (const key in obj) {
            obj[key] = cb(obj[key]);
        }
        return obj;
    } else {
        return {};
    }
}


module.exports = mapObject;