function invert(obj) {
    let newObj = {};
    if (obj) {
        for (const key in obj) {
            newObj[obj[key]] = key;
        }
    }
    return newObj;
}


module.exports = invert;