function values(obj) {
  const arr = [];
  if (obj && typeof obj == 'object') {
    for (const key in obj) {
      arr.push(obj[key]);
    }
  }
  return arr;
}

module.exports = values;