
function pairs(obj) {
  const arr = [];
  if (obj) {
    for (const key in obj) {
      arr.push([key, obj[key]]);
    }
  }
  return arr;
}

module.exports = pairs;
